/***************************************************************************
 *   Copyright (C) 2009 by Zachary T Welch <zw@superlucidity.net>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef HELPER_OBJECT_H
#define HELPER_OBJECT_H

#include <helper/log.h>

struct driver;

#define OBJECT_SETUP_COMMAND(name) \
		COMMAND_HELPER(name, struct object *object)

struct object {
	/**
	 * The next object in a linked list.
	 */
	struct object *next;
	/**
	 * The unique numeric identifier of this instance.
	 */
	unsigned id;

	/**
	 * The unique name for this instance.
	 */
	char *name;

	/**
	 * The driver associated with this instance.
	 */
	struct driver *driver;
	/**
	 * Per-object driver state.
	 */
	void *driver_priv;
};

void *object_new(size_t object_size, const char *name, struct driver *driver);
void object_add(struct object **head, struct object *bank);

struct driver {
	struct object object;

	/**
	 * Called after creation to prepare an object for further use.
	 */
	OBJECT_SETUP_COMMAND((*setup));

	/**
	 * An array of driver-specific commands to register.  When called
	 * during the "flash bank" command, the driver can register addition
	 * commands to support new flash chip functions.
	 */
	const struct command_registration *commands;
};


#endif // HELPER_OBJECT_H
