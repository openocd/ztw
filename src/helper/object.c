/***************************************************************************
 *   Copyright (C) 2009 by Zachary T Welch <zw@superlucidity.net>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "object.h"

void *object_new(size_t object_size, const char *name, struct driver *driver)
{
	struct object *object = calloc(1, object_size);
	if (NULL == object)
		return NULL;

	object->name = strdup(name);
	if (NULL == object->name)
	{
		free(object);
		return NULL;
	}

	object->driver = driver;
	object->next = NULL;

	return object;
}

void object_add(struct object **head, struct object *object)
{
	unsigned index = 0;
	if (NULL != *head)
	{
		struct object *p = *head;
		while (NULL != p->next)
		{
			index += 1;
			p = p->next;
		}
		p->next = object;
		index += 1;
	}
	else
		*head = object;

	object->id = index;
}
