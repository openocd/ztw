/***************************************************************************
 *   Copyright (C) 2005 by Dominic Rath <Dominic.Rath@gmx.de>              *
 *   Copyright (C) 2007,2008 Øyvind Harboe <oyvind.harboe@zylin.com>       *
 *   Copyright (C) 2008 by Spencer Oliver <spen@spen-soft.co.uk>           *
 *   Copyright (C) 2009 Zachary T Welch <zw@superlucidity.net>             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef FLASH_NOR_DRIVER_H
#define FLASH_NOR_DRIVER_H

struct flash_bank;

#include <flash/common.h>

/**
 * @brief Provides the implementation-independent structure that defines
 * all of the callbacks required by OpenOCD flash drivers.
 *
 * Driver authors must implement the routines defined here, providing an
 * instance with the fields filled out.  After that, the instance must
 * be registered in flash.c, so it can be used by the driver lookup system.
 *
 * Specifically, the user can issue the command: @par
 * @code
 * flash bank name DRIVERNAME ...parameters...
 * @endcode
 * The arguments are: @par
 * @code
 * CMD_ARGV[0] = bank name
 * CMD_ARGV[1] = driver name
 * CMD_ARGV[2] = base address
 * CMD_ARGV[3] = length bytes
 * CMD_ARGV[4] = chip width in bytes
 * CMD_ARGV[5] = bus width in bytes
 * CMD_ARGV[6] = target id/name
 * CMD_ARGV[7] = driver-specific parameters
 * @endcode
 *
 * OpenOCD will search for the driver with a @c flash_driver_s::name
 * that matches @c DRIVERNAME.
 *
 * The flash subsystem calls some of the other drivers routines a using
 * corresponding static <code>flash_driver_<i>callback</i>()</code>
 * routine in flash.c.
 *
 * For example, CMD_ARGV[4] = 16 bit flash, CMD_ARGV[5] = 32bit bus.
 *
 * If extra arguments are provided (@a CMD_ARGC > 7), they will
 * start in @a CMD_ARGV[7].  These can be used to implement
 * driver-specific extensions.
 *
 * @returns ERROR_OK if successful; otherwise, an error code.
 */

struct flash_driver
{
	/// common driver characteristics
	struct driver driver;

	/**
	 * Bank/sector erase routine (target-specific).  When
	 * called, the flash driver should erase the specified sectors
	 * using whatever means are at its disposal.
	 *
	 * @param bank The bank of flash to be erased.
	 * @param first The number of the first sector to erase, typically 0.
	 * @param last The number of the last sector to erase, typically N-1.
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*erase)(struct flash_bank *bank, int first, int last);

	/**
	 * Bank/sector protection routine (target-specific).
	 * When called, the driver should disable 'flash write' bits (or
	 * enable 'erase protection' bits) for the given @a bank and @a
	 * sectors.
	 *
	 * @param bank The bank to protect or unprotect.
	 * @param set If non-zero, enable protection; if 0, disable it.
	 * @param first The first sector to (un)protect, typicaly 0.
	 * @param last The last sector to (un)project, typically N-1.
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*protect)(struct flash_bank *bank, int set, int first, int last);

	/**
	 * Program data into the flash.  Note CPU address will be
	 * "bank->base + offset", while the physical address is
	 * dependent upon current target MMU mappings.
	 *
	 * @param bank The bank to program
	 * @param buffer The data bytes to write.
	 * @param offset The offset into the chip to program.
	 * @param count The number of bytes to write.
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*write)(struct flash_bank *bank,
			uint8_t *buffer, uint32_t offset, uint32_t count);

	/**
	 * Probe to determine what kind of flash is present.
	 * This is invoked by the "probe" script command.
	 *
	 * @param bank The bank to probe
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*probe)(struct flash_bank *bank);

	/**
	 * Check the erasure status of a flash bank.
	 * When called, the driver routine must perform the required
	 * checks and then set the @c flash_sector_s::is_erased field
	 * for each of the flash banks's sectors.
	 *
	 * @param bank The bank to check
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*erase_check)(struct flash_bank *bank);

	/**
	 * Determine if the specific bank is "protected" or not.
	 * When called, the driver routine must must perform the
	 * required protection check(s) and then set the @c
	 * flash_sector_s::is_protected field for each of the flash
	 * bank's sectors.
	 *
	 * @param bank - the bank to check
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*protect_check)(struct flash_bank *bank);

	/**
	 * Display human-readable information about the flash
	 * bank into the given buffer.  Drivers must be careful to avoid
	 * overflowing the buffer.
	 *
	 * @param bank - the bank to get info about
	 * @param char - where to put the text for the human to read
	 * @param buf_size - the size of the human buffer.
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*info)(struct flash_bank *bank, char *buf, int buf_size);

	/**
	 * A more gentle flavor of filash_driver_s::probe, performing
	 * setup with less noise.  Generally, driver routines should test
	 * to seee if the bank has already been probed; if it has, the
	 * driver probably should not perform its probe a second time.
	 *
	 * This callback is often called from the inside of other
	 * routines (e.g. GDB flash downloads) to autoprobe the flash as
	 * it is programing the flash.
	 *
	 * @param bank - the bank to probe
	 * @returns ERROR_OK if successful; otherwise, an error code.
	 */
	int (*auto_probe)(struct flash_bank *bank);
};

#define FLASH_BANK_COMMAND_HANDLER(name) static OBJECT_SETUP_COMMAND(name)

#define flash_driver_name(__driver) (__driver)->driver.object.name
#define flash_driver_commands(__driver) (__driver)->driver.commands
#define flash_driver_bank_command(__driver) (__driver)->driver.setup

#define FLASH_DRIVER(__name, __setup, __commands, __args...) \
	struct flash_driver __name##_flash = { \
		.driver = { \
			.object = { \
				.name = stringify(__name), \
			}, \
			.setup = __setup, \
			.commands = __commands, \
		}, \
		##__args \
	};

/**
 * Find a NOR flash driver by its name.
 * @param name The name of the requested driver.
 * @returns The flash_driver called @c name, or NULL if not found.
 */
struct flash_driver *flash_driver_find_by_name(const char *name);

static inline struct flash_driver *
flash_driver_from_driver(struct driver *driver)
{
	return container_of(driver, struct flash_driver, driver);
}

#endif // FLASH_NOR_DRIVER_H
