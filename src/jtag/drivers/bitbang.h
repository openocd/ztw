/***************************************************************************
 *   Copyright (C) 2005 by Dominic Rath                                    *
 *   Dominic.Rath@gmx.de                                                   *
 *                                                                         *
 *   Copyright (C) 2007,2008 Øyvind Harboe                                 *
 *   oyvind.harboe@zylin.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef BITBANG_H
#define BITBANG_H

#include <jtag/interface.h>

struct jtag_device;
struct bitbang_device;

/**
 * Defines the set of low-level callbacks for bit-bang devices.
 */
struct bitbang_interface {
	int (*read)(struct bitbang_device *interface);
	void (*write)(struct bitbang_device *interface,
			bool tck, bool tms, bool tdi);
	void (*reset)(struct bitbang_device *interface, bool trst, bool srst);
	void (*blink)(struct bitbang_device *interface, bool on);
};

struct bitbang_device {
	struct bitbang_interface *driver;
};

static inline
struct bitbang_device *bitbang_from_jtag_device(struct jtag_device *jtag)
{
	return (struct bitbang_device *)jtag_device_data(jtag);
}


int bitbang_execute_queue(struct jtag_device *jtag);

#endif /* BITBANG_H */
