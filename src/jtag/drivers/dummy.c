/***************************************************************************
 *   Copyright (C) 2008 by Øyvind Harboe                                   *
 *   oyvind.harboe@zylin.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <jtag/interface.h>
#include "bitbang.h"
#include "hello.h"

struct dummy_device {
	// the dummy device is a bitbang device
	struct bitbang_device bitbang;

	/* private tap controller state, which tracks state for calling code */
	tap_state_t tap_state;
	/* edge detector */
	int clock;         
	/** count clocks in any stable state, only stable states */
	int count;         

	uint32_t data;
};

static struct dummy_device *dummy_device_from_bitbang(
		struct bitbang_device *device)
{
	return container_of(device, struct dummy_device, bitbang);
}

static int dummy_read(struct bitbang_device *bitbang)
{
	struct dummy_device *state = dummy_device_from_bitbang(bitbang);
	int data = 1 & state->data;
	state->data = (state->data >> 1) | (1 << 31);
	return data;
}


static void dummy_write(struct bitbang_device *bitbang,
		bool tck, bool tms, bool tdi)
{
	struct dummy_device *state = dummy_device_from_bitbang(bitbang);
	if (tck == state->clock)
		return;

	state->clock = tck;

	/* TAP standard: "state transitions occur on rising edge of clock" */
	if (!state->clock)
		return;

	tap_state_t old_state = state->tap_state;
	state->tap_state = tap_state_transition(old_state, tms);

	if (old_state == state->tap_state)
	{
		/**
		 * this is a stable state clock edge, no change of state here,
		 * simply increment clock_count for subsequent logging
		 */
		++state->count;
	}
	if (state->count)
	{
		LOG_DEBUG("dummy_tap: %d stable clocks", state->count);
		state->count = 0;
	}

	LOG_DEBUG("dummy_tap: %s", tap_state_name(state->tap_state));

#if defined(DEBUG)
	if (state->tap_state == TAP_DRCAPTURE)
		state->data = 0x01255043;
#endif
}

static void dummy_reset(struct bitbang_device *bitbang, bool trst, bool srst)
{
	struct dummy_device *state = dummy_device_from_bitbang(bitbang);
	state->clock = 0;

	if (trst || (srst && (jtag_get_reset_config() & RESET_SRST_PULLS_TRST)))
		state->tap_state = TAP_RESET;

	LOG_DEBUG("reset to: %s", tap_state_name(state->tap_state));
}

static void dummy_led(struct bitbang_device *bitbang, bool on)
{
	// no-op
}

static struct bitbang_interface dummy_bitbang = {
		.read = &dummy_read,
		.write = &dummy_write,
		.reset = &dummy_reset,
		.blink = &dummy_led,
	};


static int dummy_khz(struct jtag_device *interface, int khz, int *jtag_speed)
{
	if (khz == 0)
	{
		*jtag_speed = 0;
	}
	else
	{
		*jtag_speed = 64000/khz;
	}
	return ERROR_OK;
}

static int dummy_speed_div(struct jtag_device *interface, int speed, int *khz)
{
	if (speed == 0)
	{
		*khz = 0;
	}
	else
	{
		*khz = 64000/speed;
	}

	return ERROR_OK;
}

static int dummy_speed(struct jtag_device *interface, int speed)
{
	return ERROR_OK;
}

static int dummy_init(struct jtag_device *interface)
{
	struct dummy_device *state = calloc(1, sizeof(*state));
	if (NULL == state)
		return -ENOMEM;

	state->bitbang.driver = &dummy_bitbang;
	state->tap_state = TAP_RESET;

	set_jtag_device_data(interface, state);

	return ERROR_OK;
}

static int dummy_quit(struct jtag_device *interface)
{
	return ERROR_OK;
}

static const struct command_registration dummy_command_handlers[] = {
	{
		.name = "dummy",
		.mode = COMMAND_ANY,
		.help = "dummy interface driver commands",

		.chain = hello_command_handlers,
	},
	COMMAND_REGISTRATION_DONE,
};

/* The dummy driver is used to easily check the code path
 * where the target is unresponsive.
 */
JTAG_INTERFACE(dummy, dummy_command_handlers,
		.execute_queue = &bitbang_execute_queue,

		.speed = &dummy_speed,
		.khz = &dummy_khz,
		.speed_div = &dummy_speed_div,

		.init = &dummy_init,
		.quit = &dummy_quit,
	);
